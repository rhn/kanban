/*************************************************************************************                                                          
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#include "taskslistmerge.h"
#include "tasksmodel.h"
#include <QDebug>

void TasksListMerge::merge(const QString& updatedPath, const QString& dataPath)
{
	QScopedPointer<TasksModel> updatedmodel(new TasksModel(updatedPath));
	QScopedPointer<const TasksModel> datamodel(new TasksModel(dataPath));
	
	QModelIndex startUp=updatedmodel->index(0,0);
	QModelIndex startData=datamodel->index(0,0);
	
	for(int i=0; i<datamodel->rowCount(); i++) {
		QStandardItem* item=datamodel->item(i);
		uint timestamp=item->data(TasksModel::TimestampRole).toUInt();
		
		QModelIndexList items=updatedmodel->match(startUp, TasksModel::TimestampRole, timestamp, 1, Qt::MatchExactly);
		
		 //We add it because its not present in the current (updatedmodel)
		if(items.isEmpty() && timestamp>updatedmodel->lastSync())
		{
			updatedmodel->addTask(item->text(),
								item->data(TasksModel::ImportantRole).toBool(),
								item->checkState()==Qt::Checked,
								timestamp);
			
			qDebug() << "new element found" << item->text() << timestamp;
		} else if(!items.isEmpty() && updatedmodel->lastSync()<datamodel->lastSync()) {//TODO: should change to per-item
			QStandardItem* other=updatedmodel->item(items.first().row(), 0);
			
			other->setData(item->data(TasksModel::ImportantRole), TasksModel::ImportantRole);
			other->setData(item->data(Qt::CheckStateRole), Qt::CheckStateRole);
		} else {
			qDebug() << "not doing anything" << items << item->text() << timestamp << updatedmodel->lastSync() << item->data(TasksModel::TimestampRole);
		}
	}
	
	for(int i=0; i<updatedmodel->rowCount(); i++) {
		QStandardItem* current=updatedmodel->item(i);
		uint timestamp=current->data(TasksModel::TimestampRole).toUInt();
		QModelIndexList items=datamodel->match(startData, TasksModel::TimestampRole, timestamp, 1, Qt::MatchExactly);
		
		if(items.isEmpty() && timestamp<datamodel->lastSync()) { //We remove it because it was removed remotely
			qDebug() << "removed" << current->text() << timestamp << datamodel->lastSync();
			updatedmodel->removeRows(i, 1);
			i--;
		}
	}
	
	updatedmodel->markSynced();
}