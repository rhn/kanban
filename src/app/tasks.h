/*************************************************************************************                                                          
 *  Copyright (C) 2009 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#ifndef TASKS_H
#define TASKS_H

#include <QMainWindow>
#include <QUrl>

class QSettings;
class QComboBox;
class QModelIndex;
class TasksModel;
class TasksView;

class Tasks : public QMainWindow
{
	Q_OBJECT
	public:
		Tasks(QWidget* parent=0);
		virtual ~Tasks();
		
	private slots:
		void removeTask();
		
		void removeList();
		void addList();
		void changeCurrentList(const QString& current);
		void sync();
		void configureSync();
		
	private:
		void setRemoteUrl(const QUrl& url);
		QUrl remoteUrl();
		
// 		QAction* m_remove;
		TasksView* m_view;
		TasksModel* m_model;
		QComboBox* m_lists;
		QAction* m_syncAction;
		QSettings* m_settings;
		QAction* m_clear;
};

#endif
