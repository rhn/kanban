/*************************************************************************************                                                          
 *  Copyright (C) 2009 by Aleix Pol <aleixpol@kde.org>                               *
 *                2018 by rhn <gihu.rhn@porcupinefatory.org>                         *
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#include "tasksmodel.h"
#include <QSet>
#include <QFile>
#include <QTextStream>
#include <QDomDocument>
#include <QDomElement>
#include <QApplication>
#include <QDebug>
#include <QPalette>
#include <QDateTime>

class TaskItem : public QStandardItem
{
	public:
		TaskItem(const QString& text, bool important, uint timestamp) : QStandardItem(text)
		{
			setCheckable(true);
			setData(important, TasksModel::ImportantRole);
			setData(timestamp, TasksModel::TimestampRole);
		}
		
		bool isDone() const { return checkState()==Qt::Checked;}
		bool isImportant() const { return data(TasksModel::ImportantRole).toBool(); }
		uint timestamp() const {  return data(TasksModel::TimestampRole).toUInt(); }
		
		void setData(const QVariant& value, int role)
		{
			switch(role) {
				case Qt::CheckStateRole: {
					bool done=value.toInt()==Qt::Checked;
					
					QFont f = font();
					f.setStrikeOut(done);
					setFont(f);
					setForeground(done ? Qt::gray : QApplication::palette().color(QPalette::WindowText));
					QStandardItem::setData(value, role);
				} break;
				case TasksModel::ImportantRole:
					if(value.toBool())
						setIcon(TasksModel::ratingIcon());
					else
						setIcon(QIcon());
					
					QStandardItem::setData(value, role);
					break;
				case Qt::EditRole:
					if(value.toString().isEmpty())
						model()->removeRow(row());
					else
						QStandardItem::setData(value, role);
					break;
				default:
					QStandardItem::setData(value, role);
			}
		}
		
		virtual bool operator<(const QStandardItem& ti) const
		{
			const TaskItem& t=(const TaskItem&) ti;
			
			if(t.isDone()!=isDone())
				return isDone();
			else if(t.isImportant()!=isImportant())
				return !isImportant();
			else
				return t.timestamp()<timestamp();
		}
};

TasksModel::TasksModel(const QString& name, QObject *parent)
	: QStandardItemModel(parent)
	, m_changes(false)
	, m_path(name)
	, m_holdSave(false)
	, m_timestamp(0)
{
	load();
	
	connect(this, SIGNAL(rowsInserted(QModelIndex, int, int)), SLOT(changesHappened()));
	connect(this, SIGNAL(rowsMoved(QModelIndex, int, int, QModelIndex, int)), SLOT(changesHappened()));
	connect(this, SIGNAL(rowsRemoved(QModelIndex, int, int)), SLOT(changesHappened()));
}

TasksModel::~TasksModel()
{
	save();
}

QModelIndex TasksModel::addTask(const QString& str, bool important, bool done, uint timestamp)
{
	TaskItem* task = new TaskItem(str, important, timestamp);

	task->setCheckState(done ? Qt::Checked : Qt::Unchecked);

	appendRow(task);
	return task->index();
}

bool TasksModel::save()
{
	if(m_holdSave || !m_changes)
		return false;
	qDebug() << "Saving to..." << m_path;
	QFile file(m_path);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return false;
	
	QTextStream out(&file);
	
	out << "<tasks timestamp='" << QString::number(m_timestamp) << "'>\n";
	for(int i=0; i<rowCount(); i++)
	{
		TaskItem* t=(TaskItem*) item(i, 0);
		out << "\t<task timestamp='" << QString::number(t->timestamp()) << "'";
		if(t->isImportant())
			out << " important='true'";
		
		if(t->isDone())
			out << " done='true'";
		
		out << '>' << t->text() << "</task>\n";
	}
	out << "</tasks>\n";
	m_changes=false;
	return true;
}

bool TasksModel::load()
{
	m_holdSave=true;
	qDebug() << "Loading..." << m_path;
	
	clear();
	
	QFile file(m_path);
	if (!file.open(QIODevice::ReadOnly)) {
		m_holdSave=false;
		return false;
	}
	
	QDomDocument doc("tasks");
	if (!doc.setContent(&file)) {
		file.close();
		m_holdSave=false;
		return false;
	}
	file.close();
	
	QDomElement docElem = doc.documentElement();
	m_timestamp=docElem.attribute("timestamp", "0").toUInt();
	
	QDomNode n = docElem.firstChild();
	for(;!n.isNull(); n = n.nextSibling()) {
		QDomElement e = n.toElement();
		if(!e.isNull()) {
			bool done=e.attribute("done", "false")=="true";
			bool important=e.attribute("important", "false")=="true";
			addTask(e.text(), important, done, e.attribute("timestamp", "0").toUInt());
		}
	}
	m_holdSave=false;
	m_changes=false;
	return true;
}

void TasksModel::removeTasks(const QList<int>& idxs)
{
	QSet<int> rowsDone;
	foreach(int r, idxs) {
		if(!rowsDone.contains(r)) {
			removeRow(r);
			rowsDone.insert(r);
			m_changes=true;
		}
	}
	m_changes=true;
}

void TasksModel::cleanDone()
{
	QList<int> todel;
	for(int i=0; i<rowCount(); )
	{
		TaskItem* t=(TaskItem*) item(i, 0);
		if(t->isDone() || t->text().isEmpty())
			takeRow(i);
		else
			++i;
	}
}

QIcon TasksModel::ratingIcon()
{
	return QIcon::fromTheme("rating", QIcon::fromTheme("statusarea_presence_online_error"));
}

uint TasksModel::lastSync() const
{
	return m_timestamp;
}

void TasksModel::markSynced()
{
	m_timestamp=QDateTime::currentDateTime().toTime_t();
	m_changes=true;
}

bool TasksModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
	m_changes=true;
	save();
	return QStandardItemModel::setData(index, value, role);
}

void TasksModel::changesHappened()
{
	m_changes=true;
}

#include "tasksmodel.moc"
