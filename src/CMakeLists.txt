include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

add_subdirectory(tests)
add_subdirectory(app)
if(KDE4_FOUND)
	add_subdirectory(plasmoid)
endif(KDE4_FOUND)

set(kanbancommon_SRCS tasksmodel.cpp tasksview.cpp taskslistmerge.cpp mergedialog.cpp listsmanagement.cpp)
qt4_automoc(${kanbancommon_SRCS})

add_library(kanbancommon SHARED ${kanbancommon_SRCS})
target_link_libraries(kanbancommon ${QT_QTCORE_LIBRARY} ${QT_QTGUI_LIBRARY} ${QT_QTXML_LIBRARY} ${QT_QTNETWORK_LIBRARY})

install(TARGETS kanbancommon LIBRARY DESTINATION lib)
