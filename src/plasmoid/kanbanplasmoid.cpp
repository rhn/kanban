/*************************************************************************************
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "kanbanplasmoid.h"

#include <QPainter>
#include <QFontMetrics>
#include <QSizeF>
#include <QLabel>
#include <QGraphicsProxyWidget>
#include <QFont>
#include <QSettings>
#include <QComboBox>
#include <QInputDialog>
#include <QMessageBox>
#include <kicon.h>
#include <kiconloader.h>
#include <kcombobox.h>
#include <kconfigdialog.h>
#include <KDE/Plasma/ToolButton>

#include <plasma/theme.h>
#include <plasma/dataengine.h>
#include <plasma/tooltipcontent.h>
#include <plasma/tooltipmanager.h>
#include <tasksview.h>
#include <tasksmodel.h>
#include <listsmanagement.h>
#include <mergedialog.h>

#include "ui_config.h"

// This is the command that links your applet to the .desktop file
K_EXPORT_PLASMA_APPLET(kanbanplasmoid, KanbanPlasmoid)

using namespace Plasma;

KanbanPlasmoid::KanbanPlasmoid(QObject *parent, const QVariantList &args)
	: PopupApplet(parent, args), m_widget(0), m_layout(0)
{
	setAspectRatioMode(IgnoreAspectRatio);
	
	m_settings=new QSettings(this);
	m_configUi = new Ui::Config;
}

KanbanPlasmoid::~KanbanPlasmoid()
{
	delete m_configUi;
}

void KanbanPlasmoid::init()
{
	setPopupIcon("kanban");
}

void KanbanPlasmoid::addAction(QGraphicsLinearLayout* layout, const KIcon& icon, const QString& text, QObject* recv, const char* slot)
{
	ToolButton* b = new Plasma::ToolButton;
	b->setIcon(icon);
	b->setToolTip(text);
	connect(b, SIGNAL(clicked()), recv, slot);
	
	layout->addItem(b);
}

QGraphicsWidget* KanbanPlasmoid::graphicsWidget()
{
	if(!m_widget) {
		m_widget = new QGraphicsWidget(this);
		
		m_model = new TasksModel(ListsManagement::location(tr("default")), this);
		
		m_view = new TasksView(0);
		m_view->setAttribute(Qt::WA_NoSystemBackground);
		m_view->setModel(m_model);
		QGraphicsProxyWidget* proxy = new QGraphicsProxyWidget(m_widget);
		proxy->setWidget(m_view);
		
		m_lists=new Plasma::ComboBox;
		m_lists->setFocusPolicy(Qt::NoFocus);
		m_lists->nativeWidget()->addItems(ListsManagement::availableLists());
		m_lists->nativeWidget()->setCurrentIndex(m_lists->nativeWidget()->findText(tr("default")));
		connect(m_lists->nativeWidget(), SIGNAL(currentIndexChanged(QString)), SLOT(changeCurrentList(QString)));
		
		m_layout = new QGraphicsLinearLayout(m_widget);
		m_layout->setOrientation(Qt::Vertical);
		
		QGraphicsLinearLayout* actions = new QGraphicsLinearLayout;
		addAction(actions, KIcon("edit-clear"), tr("Clear"), m_view, SIGNAL(cleanDone()));
		actions->addItem(m_lists);
		addAction(actions, KIcon("folder-sync"), tr("Synchronize"), this, SLOT(sync()));
		addAction(actions, KIcon("document-new"), tr("New List"), this, SLOT(addList()));
		addAction(actions, KIcon("archive-remove"), tr("Remove List"), this, SLOT(removeList()));
		
		m_layout->addItem(actions);
		m_layout->addItem(proxy);
		m_widget->setPreferredSize(300,300);
	}
	m_view->setFocus();
	
	return m_widget;
}

void KanbanPlasmoid::addList()
{
	QString newName=QInputDialog::getText(0, tr("New list"), tr("What do you want the new list to be called?"));
	if(!newName.isEmpty()) {
		m_lists->addItem(newName);
		
		changeCurrentList(newName);
	}
}

void KanbanPlasmoid::changeCurrentList(const QString& newlist)
{
	if(newlist.isEmpty() || !m_view->model())
		return;
	
	QAbstractItemModel* m=m_model;
	m_model = new TasksModel(ListsManagement::location(newlist), this);
	m_view->setModel(m_model);
	
	int idx=m_lists->nativeWidget()->findText(newlist, Qt::MatchCaseSensitive);
	if(idx>=0) //if exists
		m_lists->nativeWidget()->setCurrentIndex(idx);
	else
		m_lists->nativeWidget()->setCurrentIndex(m_lists->nativeWidget()->findText(tr("default"), Qt::MatchCaseSensitive));
	
	delete m;
}

void KanbanPlasmoid::sync()
{
	if(!m_settings->contains("syncUrl")) {
		showConfigurationInterface();
		return;
	}
	
	QString selected=m_lists->nativeWidget()->currentText();
	m_model->save();
	
	MergeDialog d(m_settings->value("syncUrl").toUrl());
	d.exec();
	
	m_lists->blockSignals(true);
	m_lists->clear();
	m_lists->blockSignals(false);
	m_lists->nativeWidget()->addItems(ListsManagement::availableLists());
	changeCurrentList(selected);
}

void KanbanPlasmoid::createConfigurationInterface(KConfigDialog* parent)
{
	QWidget *widget = new QWidget(parent);
	m_configUi->setupUi(widget);
	m_configUi->syncServer->setUrl(m_settings->value("syncUrl").toUrl());
	
	parent->addPage(widget, i18n("General"), "Kanban");
	connect(parent, SIGNAL(applyClicked()), this, SLOT(configAccepted()));
	connect(parent, SIGNAL(okClicked()), this, SLOT(configAccepted()));
}

void KanbanPlasmoid::configAccepted()
{
	m_settings->setValue("syncUrl", QUrl(m_configUi->syncServer->url()));
}

void KanbanPlasmoid::removeList()
{
	QString oldList=m_lists->nativeWidget()->currentText();
	int res=QMessageBox::warning(0, tr("Remove List"), tr("Are you sure you want to remove the %1 list?").arg(oldList), 
								 QMessageBox::Yes|QMessageBox::No);
	if(res==QMessageBox::Yes) {
		m_lists->nativeWidget()->removeItem(m_lists->nativeWidget()->currentIndex());
		
		ListsManagement::removeList(oldList);
	}
}
