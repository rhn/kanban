/*************************************************************************************                                                          
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#include "mergedialog.h"
#include <QDir>
#include <QDebug>
#include <QFtp>
#include "listsmanagement.h"
#include "taskslistmerge.h"

MergeDialog::MergeDialog(const QUrl& url, QWidget* parent)
	: QProgressDialog(parent), m_url(url)
{
	QDir().mkpath(ListsManagement::remoteCache(QString()));
	
	m_remote = new QFtp(this);
	m_remote->connectToHost(m_url.host(), m_url.port(21));
	m_remote->login(m_url.userName(), m_url.password());
	
	connect(m_remote, SIGNAL(commandFinished(int,bool)), SLOT(commandFinished(int,bool)));
	connect(m_remote, SIGNAL(listInfo(QUrlInfo)), SLOT(listInfo(QUrlInfo)));
	connect(this, SIGNAL(canceled()), SLOT(canceled()));
	
	setLabelText(tr("Logging in..."));
	setMinimum(0);
	setMaximum(5);
}

void MergeDialog::canceled()
{
	m_remote->clearPendingCommands();
}

void MergeDialog::commandFinished(int id, bool error)
{
// 	qDebug() << "xxxxxxx" << id << error << m_remote->currentCommand();
	
	if(error)
		setLabelText(tr("Error: %1").arg(m_remote->errorString()));
	else switch(m_remote->currentCommand())
	{
		case QFtp::Login:
			m_remote->list(m_url.path());
			setLabelText(tr("Listing..."));
			setValue(1);
			break;
		case QFtp::List:
			if(m_downloading.isEmpty())
				performLocalSync();
			else {
				setLabelText(tr("Downloading files..."));
				setValue(2);
			}
			break;
		case QFtp::Get:
			if(!m_remote->hasPendingCommands())
				performLocalSync();
			break;
		case QFtp::Put:
			m_uploads--;
			if(m_uploads==0) {
				setLabelText(tr("Done."));
				accept();
			}
			break;
		default:
			break;
	}
}

void MergeDialog::listInfo(const QUrlInfo& item)
{
	if(item.name()=="." || item.name()=="..")
		return;
	
	qDebug() << "downloading..." << ListsManagement::remoteCache(item.name());
	QFile* f=new QFile(ListsManagement::remoteCache(item.name()));
	bool b=f->open(QFile::Text | QFile::WriteOnly);
	Q_ASSERT(b);
	
	m_downloading.append(f);
	m_remote->get(m_url.path()+'/'+item.name(), m_downloading.last(), QFtp::Ascii);
}

void MergeDialog::performLocalSync()
{
	setValue(3);
	setLabelText(tr("Merging..."));
	qDeleteAll(m_downloading);
	m_downloading.clear();
	
	QDir local(ListsManagement::location(QString())), remote(ListsManagement::remoteCache(QString()));
	QStringList localLists=local.entryList(QDir::NoDotAndDotDot | QDir::Files);
	QStringList remoteLists=remote.entryList(QDir::NoDotAndDotDot | QDir::Files);
	
	foreach(const QString& remoteList, remoteLists) {
		QFile f(ListsManagement::remoteCache(remoteList));
		if(!localLists.contains(remoteList)) {
			qDebug() << "found new list: "<< remoteList;
			bool b=f.copy(ListsManagement::location(remoteList));
			if(!b)
				setLabelText(tr("Could not copy file"));
		} else {
			qDebug() << "merging: "<< remoteList;
			TasksListMerge::merge(ListsManagement::location(remoteList), ListsManagement::remoteCache(remoteList));
		}
		bool b=f.remove();
		if(!b)
			setLabelText(tr("Could not remove file"));
	}
	
	setValue(4);
	//upload results
	setLabelText(tr("Uploading..."));
	m_uploads=0;
	foreach(const QString& file, local.entryList(QDir::NoDotAndDotDot | QDir::Files)) {
		QFile f(ListsManagement::location(file));
		bool b=f.open(QFile::ReadOnly | QFile::Text);
		Q_ASSERT(b);
		
		QByteArray content=f.readAll();
		m_remote->put(content, m_url.path()+'/'+file, QFtp::Ascii);
		
		qDebug() << "putting... " << ListsManagement::location(file) << content;
		m_uploads++;
	}
	m_remote->close();
	
	if(m_uploads==0) {
		setLabelText(tr("Done."));
		accept();
	}
}

#include "mergedialog.moc"
