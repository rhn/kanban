Kanban
======

Note-taking app.

Installation
------------

```
docker \
    run --rm -it \
    --privileged \
    -v `pwd`:/scratchbox/users/admin/home/admin/src \
    accupara/maemo \
    /bin/bash
```

Start scratchbox and install cmake:

```
sudo /scratchbox/sbin/sbox_ctl start
sudo /scratchbox/sbin/sbox_sync
sb-conf select FREMANTLE_ARMEL
/scratchbox/login apt-get install cmake
```

Make package:

```
/scratchbox/login sh
cd src/kanban/
dpkg-buildpackage
```
